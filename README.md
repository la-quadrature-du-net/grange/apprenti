![Apprenti](https://git.laquadrature.net/uploads/-/system/project/avatar/255/DWaIwQwXUAESub1.jpg-large.jpg?width=512)

Thème Wordpress du site de La Quadrature Du Net. Basé sur le thème Wisteria, créé par Marne.

La documentation du site La Quadrature se trouve dans la [documentation de l'infrastructure](https://git.laquadrature.net/lqdn-interne/infrastructure-docs/-/blob/master/services-docs/grange/site-web.md)

----

Ne pas hésiter à se référer au [Codex](https://codex.wordpress.org/) pour
toute info sur le développement de thèmes (ou le fonctionnement de
Wordpress de manière générale), en particulier la page sur le
[développement de thèmes](https://codex.wordpress.org/Theme_Development)

Le répertoire scripts/ contient quelques utilitaires nécessaire à la mise
en ligne du site ou à réaliser quelques opérations de maintenance. Il
s'agit essentiellement de scripts bash.

# Installation d'un nouveau site

Pour tester le thème, le plus simple est de créer un nouveau site sur la
grange. Pour se faciliter la vie, l'URL de test sera [https://<nom du
theme>.unegrange.dev.lqdn.fr]().

Un fichier site.yml contenant les variables nécessaire au fonctionnement
du site — incluant notamment la liste des plugins — doit être rempli. Les
variables nécessaires et leur significations sont détaillées ci-dessous.

Attention, lorsque ce fichier est mis à jour sur la branche preprod du
gitlab, un déploiement en test aura lieu, ainsi que le lancement de tests.
Une mise à jour dans la branche master amènera à un déploiement en
production.

# Ajout d'une nouvelle page de thème

Quand vous avez besoin d'une nouvelle page avec un CSS particulier qui ne s'applique que à cette page, vous aurez besoin de créer un *template*. Par exemple, voir la page empire-vsa.php.

Pour se faire, suivez les étapes : 

1. Dupliquez la page empire-vsa.php, et renommez-là pour lui donner le nom du template que vous souhaitez. Par exemple, `page-exemple.php`.
2. Créez un fichier suivant le nom que vous vennez de donner à votre page de thème, mais avec l'extension en `.css`. Par exemple, `page-exemple.css`.
3. Ouvrez le fichier en `.php`, et trouvez la ligne suivante : 

```
Template name: EmpireVSA
```

Remplacez EmpireVSA par le nom que vous voulez donner à votre thème. Éditez également le commentaire un peu plus bas avec des informations sur votre page de thème.

4. Trouvez ensuite la ligne qui commence par `wp_register_style` et modifiez chaque occurence de `empire-vsa` par le nom de votre thème.
5. Faite de même pour la ligne avec `wp_enqueue_style`.
6. Faite de même pour le nom de la function `mytheme_enque_style`, n'oubliez pas la ligne qui commence par `add_action`. 
7. Vous pouvez maintenant ajouter votre CSS à votre fichier de thème CSS, par exemple `page-exemple.css`, et cette feuille de style sera chargée en dernière par la page de thème, garantissant qu'elle soit bien appliquée. 
