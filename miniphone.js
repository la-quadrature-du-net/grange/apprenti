// Liste des députés
var meps = [];

// Valeur par défaut du pays et groupes selectionnés
var country = 0;
var group = 0;

// Pays et groupes possibles
var countries = [];
var groups = [];

// Députés sélectionnées
var selected = [];
var currentMEP = 0;

// Indique le lieu où les députés sont en fonction du jour
var place = "Bruss";
var strDates = [[14,15,16,17,18],[11,12,13,14,15], [11,12,13,14,15,25,26,27,28,29],[15,16,17,18,19],[],[],[],[],[],[],[],[]];
var now = new Date(Date.now());
if (strDates[now.getMonth()].indexOf(now.getDate()) != -1)
	place = "Str";

// L'adresse du JSON contenant les informations sur les députés
// var url = 'https://campaign.lqdn.fr/campaigns/6/contacts/?format=json';
var url = "https://www.laquadrature.net/wp-content/themes/apprenti/all_meps_2019.json"

// Récupère les informations sur les députés au lancement de la page
onload = function ()
{
	// Établie une connexion avec le serveur
	var server = new XMLHttpRequest();
	server.open("GET", url, true);
	server.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

	// Recupère la page
	server.send();

	// Lorsque la réponse à la requête est reçue
	server.onreadystatechange = function()
	{

		if (this.readyState == XMLHttpRequest.DONE && this.status == 200)
		{
			// Récupère la page
			var info = JSON.parse(server.responseText);

			// Construit l'affichage
			construct(info);
		}
	}
}

/* Construit les infos récupérées */
function construct(info)
{
	for (var i = 0; i < info.length; i++)
	{
		var mep = {};
		mep.name = info[i].last_name;
		mep.firstName = info[i].first_name;
		mep.phone1 = info[i].phone1;
		mep.phone2 = info[i].phone2;
		mep.photo = info[i].photo;
		mep.email = info[i].email;
		mep.twitter = info[i].twitter;
		mep.group = info[i].group;
		mep.country = info[i].country.toUpperCase();

		meps.push(mep);

		if (countries.indexOf(mep.country) == -1)
			countries.push(mep.country);

		if (groups.indexOf(mep.group) == -1)
			groups.push(mep.group);
	}

	// Remplit le selecteur de pays
	countries.sort();

	for (var i = 0; i < countries.length; i++)
	{
		var option = document.createElement("option");
			option.value = countries[i];
			option.innerHTML = countries[i];
			document.getElementById("select_country").appendChild(option);
	}

	document.getElementById("select_country").onchange = function()
	{
		country = this.value;
		filter();
	}


	// Remplit le selecteur de groupe
	groups.sort();

	for (var i = 0; i < groups.length; i++)
	{
		var option = document.createElement("option");
			option.value = groups[i];
			option.innerHTML = groups[i];
			document.getElementById("select_group").appendChild(option);
	}

	document.getElementById("select_group").onchange = function()
	{
		group = this.value;
		filter();
	}

	// Lance le premier filtre
	filter();

}

// Filtre en fonction du pays/groupe
function filter()
{

	selected = [];

	// Recupère la liste de députés correspondant au filtre
	for (var i = 0; i < meps.length; i++)
	{
		if ( (group == 0 || meps[i].group == group)
		&& 	 (country == 0 || meps[i].country == country) )
		{
			selected.push(meps[i]);
		}
	}

	// Met à jour le nombre de députés indiqués dans le filtre
	document.getElementById("mep_total").innerHTML = selected.length;

	// Choisit un député au hasard dans le groupe
	currentMEP = Math.floor(Math.random()*selected.length);

	// Masque/affiche les info sur le député si la liste est vide ou non
	if (selected.length)
	{
		document.getElementById("mep_info").style.display = "block";
		shuffle();
	}
	else
		empty();
}

// Sélecctionne un nouveau député
function shuffle()
{
	currentMEP++;
	if (currentMEP == selected.length)
		currentMEP = 0;

	// Met à jour les informations à afficher
	update();
}

document.getElementById("mep_next").onclick = shuffle;

// Met à jour les informations à afficher
function update()
{
	var mep = selected[currentMEP];
	document.getElementById("mep_name").innerHTML = mep.firstName + " " + mep.name;
	document.getElementById("mep_photo").style.backgroundImage = "url('"+mep.photo+"')";
	document.getElementById("mep_country").innerHTML = mep.country;
	document.getElementById("mep_group").innerHTML = mep.group;
	document.getElementById("mep_mail").innerHTML = "<a href='mailto:" + mep.email + "'>"+ mep.email + "</a>";
	document.getElementById("mep_twi").innerHTML = "<a href=https://twitter.com/" + mep.twitter.substr(1) + ">"+ mep.twitter + "</a>";

	if (place == "Bruss")
		document.getElementById("mep_tel").innerHTML = "<a href='tel:" + mep.phone1 + "'>"+ mep.phone1 + "</a>";
	else
		document.getElementById("mep_tel").innerHTML = "<a href='tel:" + mep.phone2 + "'>"+ mep.phone2 + "</a>";

	if (mep.twitter.length == 1)
		document.getElementById("mep_twi").style.display = "none";
	else
		document.getElementById("mep_twi").style.display = "inline";
}

// Masque les informations (appelé quand aucun député ne correspond au filtre)
function empty()
{
	document.getElementById("mep_photo").style.backgroundImage = "url()";
	document.getElementById("mep_info").style.display = "none";
}
