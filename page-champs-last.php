<?php
/**
Template name: Page Champs-Last
 */

get_header(); ?>

	<div class="container">
		<div class="row">
			<section id="primary" class="content-area <?php apprenti_layout_class( 'content' ); ?>">
				<main id="main" class="site-main" role="main">
					<div id="post-wrapper" class="post-wrapper post-wrapper-single post-wrapper-page">
						<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							<div class="entry-header-wrapper entry-header-wrapper-single">
								<header class="entry-header entry-header-single">
									<?php the_title( '<h1 class="title-champs">', '</h1>' ); ?>
								</header><!-- .entry-header -->
								<div class="menuchamps">
									<?php
/* Get the Page Slug to Use as a Body Class, this will only return a value on pages! */
$class = '';
/* is it a page */
if( is_page() ) { 
	global $post;
        /* Get an array of Ancestors and Parents if they exist */
	$parents = get_post_ancestors( $post->ID );
        /* Get the top Level page->ID count base 1, array base 0 so -1 */ 
	$id = ($parents) ? $parents[count($parents)-1]: $post->ID;
	/* Get the parent and set the $class with the page slug (post_name) */
        $parent = get_post( $id );
	$class = $parent->post_name;
}
?>
									<div class="analyse"><a href="<?php  echo esc_url( home_url( '/' ) ); echo $class ;?>"><?php esc_html_e('Analysis', 'apprenti')?></a></div>
									<div id="selected" class="lastart"><a href="#"><?php esc_html_e('Last articles', 'apprenti')?></a></div>
									<div class="revue"><a href="<?php  echo esc_url( home_url( '/' ) ); echo $class ;?>/RP"><?php esc_html_e('Press', 'apprenti')?></a></div>
								</div>
							</div>
							<div class="entry-content entry-content-single">
		
<?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;  query_posts("cat=-61&paged=$paged&category_name=$class"); ?> 
<?php while (have_posts()) : the_post(); ?>
								<?php insecable();?>
								<?php get_template_part( 'template-parts/content', get_post_format() );
								// End the loop.
								endwhile;
								apprenti_the_posts_pagination(); ?>
							</div><!-- .entry-content -->
						</article>
					</div><!-- .post-wrapper -->
				</main><!-- #main -->
			</section><!-- #primary -->
			<?php get_sidebar(); ?>
		</div><!-- .row -->
	</div><!-- .container -->

<?php get_footer(); ?>