<?php
/**
 * Template part for displaying single posts.
 *
 * @package apprenti
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="entry-header-wrapper entry-header-wrapper-single">
		<div class="entry-meta entry-meta-single entry-meta-header-before">
			<?php
			
		//	apprenti_posted_by();
			apprenti_post_edit_link();
			?>
		</div><!-- .entry-meta -->

		<header class="entry-header entry-header-single">
			<?php the_title( '<h1 class="entry-title entry-title-single">', '</h1>' );  apprenti_posted_on();?>
		</header><!-- .entry-header -->
	</div><!-- .entry-header-wrapper -->
	<br>

<!-- Image en haut d'article (haute résolution !) -->
<!-- <?php apprenti_featured_image(); ?> -->
<?php

	if (has_post_thumbnail())
	{
		echo '<div class="entry-image-wrapper entry-image-wrapper-archive"><div class="post-thumbnail" style="background-image:url(';
		echo get_the_post_thumbnail_url( null, "full");
		echo ');background-color: #cccccc;"></div></div>';
	}
?>


	<br>
	<div class="entry-content entry-content-single">
		<?php the_content(); ?>
		<?php
			wp_link_pages( array(
				'before'      => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'apprenti' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
			) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-meta entry-meta-single entry-meta-footer">
		<div id="postedans">
		<p><?php esc_html_e('Posted in', 'apprenti')?>
<?php

function e_href($url,$com)
{
    // $lien = $home."/".$com;
    // Fixed by Nono, np@laquadrature.net
    // $home n'est pas défini
	$lien = "/".$com;
		echo "<a href=\"$lien\">$url</a>";
}
function print_categ($categ)
	{
	$len_categ = count($categ);
	if ($len_categ == 1) {
		e_href($categ[0]->cat_name,$categ[0]->slug);
} elseif ($len_categ ==2) {
    e_href($categ[0]->cat_name,$categ[0]->slug); echo " et " ; e_href($categ[1]->cat_name,$categ[1]->slug);
} elseif ($len_categ ==3) {
    e_href($categ[0]->cat_name,$categ[0]->slug); echo ", " ; e_href($categ[1]->cat_name,$categ[1]->slug);
	echo " et " ; e_href($categ[2]->cat_name,$categ[2]->slug);
} elseif ($len_categ ==4) {
    e_href($categ[0]->cat_name,$categ[0]->slug); echo ", " ; e_href($categ[1]->cat_name,$categ[1]->slug);
	echo ", " ; e_href($categ[2]->cat_name,$categ[2]->slug);
	echo " et " ; e_href($categ[3]->cat_name,$categ[3]->slug);
} elseif ($len_categ ==5) {
    e_href($categ[0]->cat_name,$categ[0]->slug); echo ", " ; e_href($categ[1]->cat_name,$categ[1]->slug);
	echo ", " ; e_href($categ[2]->cat_name,$categ[2]->slug);
	echo ", " ; e_href($categ[3]->cat_name,$categ[3]->slug);
	echo " et " ; e_href($categ[4]->cat_name,$categ[4]->slug);
} 
	}
?>

<?php
$category = get_the_category();
print_categ($category);
?>

		</p>
		</div>
	</footer><!-- .entry-meta -->

</article><!-- #post-## -->
