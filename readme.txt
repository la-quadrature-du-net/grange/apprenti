=== Apprenti ===
Requires at least: WordPress 4.7
Tested up to: WordPress 4.9.7
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Tags: two-columns, left-sidebar, right-sidebar, custom-background, custom-colors, custom-header, custom-menu, editor-style, featured-images, flexible-header, rtl-language-support, sticky-post, theme-options, threaded-comments, translation-ready, blog, news

== Description ==
Apprenti is based on Wisteria and modified by La Quadrature du Net.

* Mobile-first, Responsive Layout
* Custom Colors
* Custom Header
* Post Formats
* The GPL v2.0 or later license. :) Use it to make something cool.

== Notes ==
Based on Wisteria 1.1.3 https://wpfriendship.com/wisteria