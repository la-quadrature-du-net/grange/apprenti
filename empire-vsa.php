<?php
/**
Template name: EmpireVSA
 */

/*
   * Pour éviter de sur charger la feuille style.css, on va charger le CSS de ce template séparément.
   * Cette méthode va charger la feuille de style en dernier, ce qui fait qu'elle sera prédominante.
   * La feuille de style as le même nom que le modèle de page. 
   * nono <nono@laquadrature.net> 2024-04-25 
   */

// On charge la page de style à partir du dossier actuel.
// La fonction get_template_directory_uri permet de donner un chemin relatif 
// et doit être utilisé pour éviter que la page casse si on la charge depuis un 
// autre chemin que celui par défaut.
wp_register_style( "empire-vsa-css", get_template_directory_uri() . '/empire-vsa.css');


// Cette fonction va ajouter la page de style à la queue de chargement de ressource de WP
// et assure que cette feuille sera bien la dernière à être chargée, garantissant sa bonne
// application.
function mytheme_enqueue_style() {
    wp_enqueue_style( 'empire-vsa-css', get_stylesheet_uri() );
}

// On ajoute l'action au moment du rendu de la page à partir du modèle.
add_action( 'wp_enqueue_scripts', 'mytheme_enqueue_style' );

// La partie ci dessus est tiré de la page https://webdesign.tutsplus.com/loading-css-into-wordpress-the-right-way--cms-20402t

/*
   * Wordpress as tendance as rajouter des balises <p> dans l'édition HTML. 
   * Le code ci-dessous supprime cette fonctionnalitée.
   */

function disable_wp_auto_p( $content ) {
  if ( is_singular( 'page' ) ) {
    remove_filter( 'the_content', 'wpautop' );
    remove_filter( 'the_excerpt', 'wpautop' );
  }
  return $content;
}

add_filter( 'the_content', 'disable_wp_auto_p', 0 );

/******/

get_header(); ?>

	<div class="container">
		<div class="row">

			<section id="primary" class="content-area <?php apprenti_layout_class( 'content' ); ?>">
				<main id="main" class="site-main" role="main">

					<div id="post-wrapper" class="post-wrapper post-wrapper-single post-wrapper-page">
					<?php while ( have_posts() ) : the_post(); ?>
<?php insecable();?>
						<?php get_template_part( 'template-parts/content', 'page' ); ?>

					<?php endwhile; // end of the loop. ?>
					</div><!-- .post-wrapper -->

				</main><!-- #main -->
			</section><!-- #primary -->

			<?php get_sidebar(); ?>

		</div><!-- .row -->
	</div><!-- .container -->

<?php get_footer(); ?>
