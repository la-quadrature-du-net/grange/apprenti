<?php
/**
 * apprenti Theme Customizer
 *
 * @package apprenti
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function apprenti_customize_register ( $wp_customize ) {

	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'background_color' )->transport = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

	/**
	 * Theme Options Panel
	 */
	$wp_customize->add_panel( 'apprenti_theme_options', array(
	    'title'     => esc_html__( 'Theme Options', 'apprenti' ),
	    'priority'  => 1,
	) );

	/**
	 * Upgrade Section
	 */
	$wp_customize->add_section( 'apprenti_upgrade_options', array (
		'title'     => esc_html__( 'apprenti Pro Upgrade', 'apprenti' ),
		'panel'     => 'apprenti_theme_options',
		'priority'  => 10,
		'description' => esc_html__( 'Support apprenti project by upgrading to apprenti Pro. Use 30% exclusive discount code: apprentiPRO30', 'apprenti' ),
	) );

	// Theme Info
	$wp_customize->add_setting ( 'apprenti_theme_info', array(
		'default' => '',
	) );

	$wp_customize->add_control(
		new apprenti_Button_Control(
			$wp_customize,
			'apprenti_theme_info',
			array(
				'label'         => esc_html__( 'apprenti Lite vs apprenti Pro', 'apprenti' ),
				'section'       => 'apprenti_upgrade_options',
				'priority'      => 1,
				'type'          => 'apprenti-button',
				'button_tag'    => 'a',
				'button_class'  => 'button button-primary',
				'button_href'   => 'https://wpfriendship.com/apprenti/',
				'button_target' => '_blank',
			)
		)
	);

	/**
	 * General Options Section
	 */
	$wp_customize->add_section( 'apprenti_general_options', array (
		'title'     => esc_html__( 'General Options', 'apprenti' ),
		'panel'     => 'apprenti_theme_options',
		'priority'  => 20,
		'description' => esc_html__( 'Personalize the settings of your theme.', 'apprenti' ),
	) );

	// Featured Image Size
	$wp_customize->add_setting ( 'apprenti_featured_image_size', array (
		'default'           => apprenti_default( 'apprenti_featured_image_size' ),
		'sanitize_callback' => 'apprenti_sanitize_select',
	) );

	$wp_customize->add_control ( 'apprenti_featured_image_size', array (
		'label'    => esc_html__( 'Featured Image Size', 'apprenti' ),
		'section'  => 'apprenti_general_options',
		'priority' => 1,
		'type'     => 'select',
		'choices'  => array(
			'normal' => esc_html__( 'Normal',      'apprenti'),
			'retina' => esc_html__( 'Retina (2x)', 'apprenti'),
		),
	) );

	// Main Sidebar Position
	$wp_customize->add_setting ( 'apprenti_sidebar_position', array (
		'default'           => apprenti_default( 'apprenti_sidebar_position' ),
		'sanitize_callback' => 'apprenti_sanitize_select',
	) );

	$wp_customize->add_control ( 'apprenti_sidebar_position', array (
		'label'    => esc_html__( 'Main Sidebar Position (if active)', 'apprenti' ),
		'section'  => 'apprenti_general_options',
		'priority' => 2,
		'type'     => 'select',
		'choices'  => array(
			'right' => esc_html__( 'Right', 'apprenti'),
			'left'  => esc_html__( 'Left',  'apprenti'),
		),
	) );

	/**
	 * Footer Section
	 */
	$wp_customize->add_section( 'apprenti_footer_options', array (
		'title'       => esc_html__( 'Footer Options', 'apprenti' ),
		'panel'       => 'apprenti_theme_options',
		'priority'    => 30,
		'description' => esc_html__( 'Personalize the footer settings of your theme.', 'apprenti' ),
	) );

	// Copyright Control
	$wp_customize->add_setting ( 'apprenti_copyright', array (
		'default'           => apprenti_default( 'apprenti_copyright' ),
		'transport'         => 'postMessage',
		'sanitize_callback' => 'wp_kses_post',
	) );

	$wp_customize->add_control ( 'apprenti_copyright', array (
		'label'    => esc_html__( 'Copyright', 'apprenti' ),
		'section'  => 'apprenti_footer_options',
		'priority' => 1,
		'type'     => 'textarea',
	) );

	// Credit Control
	$wp_customize->add_setting ( 'apprenti_credit', array (
		'default'           => apprenti_default( 'apprenti_credit' ),
		'transport'         => 'postMessage',
		'sanitize_callback' => 'apprenti_sanitize_checkbox',
	) );

	$wp_customize->add_control ( 'apprenti_credit', array (
		'label'    => esc_html__( 'Display Designer Credit', 'apprenti' ),
		'section'  => 'apprenti_footer_options',
		'priority' => 2,
		'type'     => 'checkbox',
	) );

	/**
	 * Support Section
	 */
	$wp_customize->add_section( 'apprenti_support_options', array(
		'title'       => esc_html__( 'Support Options', 'apprenti' ),
		'description' => esc_html__( 'Thanks for your interest in apprenti! If you have any questions or run into any trouble, please visit us the following links. We will get you fixed up!', 'apprenti' ),
		'panel'       => 'apprenti_theme_options',
		'priority'    => 40,
	) );

	// Theme Support
	$wp_customize->add_setting ( 'apprenti_theme_support', array(
		'default' => '',
	) );

	$wp_customize->add_control(
		new apprenti_Button_Control(
			$wp_customize,
			'apprenti_theme_support',
			array(
				'label'         => esc_html__( 'apprenti Support', 'apprenti' ),
				'section'       => 'apprenti_support_options',
				'priority'      => 2,
				'type'          => 'apprenti-button',
				'button_tag'    => 'a',
				'button_class'  => 'button button-primary',
				'button_href'   => 'https://wpfriendship.com/contact/',
				'button_target' => '_blank',
			)
		)
	);

	/**
	 * Review Section
	 */
	$wp_customize->add_section( 'apprenti_review_options', array(
		'title'       => esc_html__( 'Enjoying the theme?', 'apprenti' ),
		'description' => esc_html__( 'Why not leave us a review on WordPress.org? We\'d really appreciate it!', 'apprenti' ),
		'panel'       => 'apprenti_theme_options',
		'priority'    => 50,
	) );

	// Theme
	$wp_customize->add_setting ( 'apprenti_theme_review', array(
		'default' => '',
	) );

	$wp_customize->add_control(
		new apprenti_Button_Control(
			$wp_customize,
			'apprenti_theme_review',
			array(
				'label'         => esc_html__( 'Review on WordPress.org', 'apprenti' ),
				'section'       => 'apprenti_review_options',
				'type'          => 'apprenti-button',
				'button_tag'    => 'a',
				'button_class'  => 'button button-primary',
				'button_href'   => 'https://wordpress.org/support/theme/apprenti/reviews',
				'button_target' => '_blank',
			)
		)
	);
}
add_action( 'customize_register', 'apprenti_customize_register' );

/**
 * Button Control Class
 */
if ( class_exists( 'WP_Customize_Control' ) ) {

	class apprenti_Button_Control extends WP_Customize_Control {
		/**
		 * @access public
		 * @var string
		 */
		public $type = 'apprenti-button';

		/**
		 * HTML tag to render button object.
		 *
		 * @var  string
		 */
		protected $button_tag = 'button';

		/**
		 * Class to render button object.
		 *
		 * @var  string
		 */
		protected $button_class = 'button button-primary';

		/**
		 * Link for <a> based button.
		 *
		 * @var  string
		 */
		protected $button_href = 'javascript:void(0)';

		/**
		 * Target for <a> based button.
		 *
		 * @var  string
		 */
		protected $button_target = '';

		/**
		 * Click event handler.
		 *
		 * @var  string
		 */
		protected $button_onclick = '';

		/**
		 * ID attribute for HTML tab.
		 *
		 * @var  string
		 */
		protected $button_tag_id = '';

		/**
		 * Render the control's content.
		 */
		public function render_content() {
		?>
			<span class="center">
				<?php
				// Print open tag
				echo '<' . esc_html( $this->button_tag );

				// button class
				if ( ! empty( $this->button_class ) ) {
					echo ' class="' . esc_attr( $this->button_class ) . '"';
				}

				// button or href
				if ( 'button' == $this->button_tag ) {
					echo ' type="button"';
				} else {
					echo ' href="' . esc_url( $this->button_href ) . '"' . ( empty( $this->button_tag ) ? '' : ' target="' . esc_attr( $this->button_target ) . '"' );
				}

				// onClick Event
				if ( ! empty( $this->button_onclick ) ) {
					echo ' onclick="' . esc_js( $this->button_onclick ) . '"';
				}

				// ID
				if ( ! empty( $this->button_tag_id ) ) {
					echo ' id="' . esc_attr( $this->button_tag_id ) . '"';
				}

				echo '>';

				// Print text inside tag
				echo esc_html( $this->label );

				// Print close tag
				echo '</' . esc_html( $this->button_tag ) . '>';
				?>
			</span>
		<?php
		}
	}

}

/**
 * Sanitize Select.
 *
 * @param string $input Slug to sanitize.
 * @param WP_Customize_Setting $setting Setting instance.
 * @return string Sanitized slug if it is a valid choice; otherwise, the setting default.
 */
function apprenti_sanitize_select( $input, $setting ) {

	// Ensure input is a slug.
	$input = sanitize_key( $input );

	// Get list of choices from the control associated with the setting.
	$choices = $setting->manager->get_control( $setting->id )->choices;

	// If the input is a valid key, return it; otherwise, return the default.
	return ( array_key_exists( $input, $choices ) ? $input : $setting->default );
}

/**
 * Sanitize the checkbox.
 *
 * @param bool $checked Whether the checkbox is checked.
 * @return bool Whether the checkbox is checked.
 */
function apprenti_sanitize_checkbox( $checked ) {
	return ( ( isset( $checked ) && true === $checked ) ? true : false );
}

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function apprenti_customize_preview_js() {
	wp_enqueue_script( 'apprenti_customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20140120', true );
}
add_action( 'customize_preview_init', 'apprenti_customize_preview_js' );
