<?php
/**
 * The template for displaying site navigation
 * @package apprenti
 */
?>

<nav id="site-navigation" class="main-navigation" role="navigation">
	<div class="container">
		<div class="row">
			<div class="col">

				<div class="main-navigation-inside">

					
<label>
  <input id="menumobile" type="checkbox">
  <span class="menu">
    <span class="hamburger"></span>
  </span>
					<?php
					// Header Menu
					wp_nav_menu( apply_filters( 'apprenti_header_menu_args', array(
						'container'       => 'div',
						'container_class' => 'site-header-menu',
						'theme_location'  => 'header-menu',
						'menu_class'      => 'header-menu sf-menu',
						'menu_id'         => 'menu-1',
						'depth'           => 3,
					) ) );
	
	?></li>
	<?php dynamic_sidebar( 'sidebar-1' ); ?>	
	</ul>
	</div>
</label>					
				</div><!-- .main-navigation-inside -->
			</div><!-- .col -->
		</div><!-- .row -->
	</div><!-- .container -->
</nav><!-- .main-navigation -->
