<?php
/**
 * The template for displaying search forms.
 *
 * @package apprenti
 */
?>

<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<label>
		<span class="screen-reader-text"><?php echo esc_html_x( 'Search for:', 'label', 'apprenti' ); ?></span>
		<input type="search" class="search-field" autofocus placeholder="<?php echo esc_attr_x( 'Search &hellip;', 'placeholder', 'apprenti' ); ?>" value="<?php echo get_search_query(); ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label', 'apprenti' ); ?>" />
	</label>
	<button type="submit" class="search-submit"><span class="screen-reader-text"><?php echo esc_html_x( 'Search', 'submit button', 'apprenti' ); ?></span></button>
</form>
