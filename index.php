<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package apprenti
 */

get_header(); ?>

	<div class="container">
		<div class="row">
			
<section id="primary" class="content-area <?php apprenti_layout_class( 'content' ); ?>">
				<main id="main" class="site-main" role="main">	
				<?php if ( have_posts() ) : $i=0; ?>
				<?php insecable();?>
					<div id="post-wrapper" class="post-wrapper post-wrapper-archive">
					<?php /* Start the Loop */  ?>
					<?php //query_posts('cat=-61'); while ( have_posts() ) : the_post(); ?>
										<?php// while ( have_posts() ) : the_post(); ?>
			
						
<?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;  query_posts("cat=-61&paged=$paged"); ?>
<?php while (have_posts()) : the_post(); ?>
						<?php
							/* Include the Post-Format-specific template for the content.
							 * If you want to override this in a child theme, then include a file
							 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
							 */
							include(locate_template('template-parts/content.php'));
							//get_template_part( 'template-parts/content', get_post_format() );
		 // apprenti_entry_footer(); 
		 // 
?>
					

					<?php $i++; endwhile; ?>
					</div><!-- .post-wrapper -->

					<?php apprenti_the_posts_pagination(); ?>

				<?php else : ?>

				<?php get_template_part( 'template-parts/content', 'none' ); ?>


				<?php endif; ?>

				</main><!-- #main -->
			</section><!-- #primary -->

			<?php get_sidebar(); ?>

		</div><!-- .row -->
	</div><!-- .container -->

<?php get_footer(); ?>
