<?php
/**
 * The default template for displaying content
 *
 * @package apprenti
 */
?>
	<?php	$plink=get_permalink(); ?>
	<a class="link-article" href="<?php echo"$plink"; ?>">
<article class="desktop" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		
	<div class="post-content-wrapper post-content-wrapper-archive">
<?php apprenti_post_thumbnail(); ?>
		<div class="entry-data-wrapper entry-data-wrapper-archive">
			<div class="entry-header-wrapper entry-header-wrapper-archive">
				<?php if ( 'post' === get_post_type() ) : // For Posts ?>
				<div class="entry-meta entry-meta-header-before">
					<!--apprenti_post_first_category();> <?php
					apprenti_sticky_post();
					?>
				</div><!-- .entry-meta -->
				<?php endif; ?>

				<header class="entry-header">
					<?php the_title( sprintf( '<h1 class="entry-title">', esc_url( get_permalink() ) ), '</h1>' ); ?>
				</header><!-- .entry-header -->

				<?php if ( 'post' === get_post_type() ) : // For Posts ?>
				<div class="entry-meta entry-meta-header-after">
					<?php
					apprenti_posted_on();
					?>
				</div><!-- .entry-meta -->
				<?php endif; ?>
			</div><!-- .entry-header-wrapper -->

			<?php if ( apprenti_has_excerpt() ) : ?>
			<div class="entry-summary">
			<?php the_excerpt(); ?>
	<p style="color:blue;">
			
<?php
	//$category = get_the_category();
//	echo $category[0]->cat_name;
?><p>
				
				</p>
				
			</div><!-- .entry-summary -->
				
			<?php endif; ?>
				
		</div><!-- .entry-data-wrapper -->

	</div><!-- .post-content-wrapper -->
	</div>
	
</article>
		</a>
	<!-- #post-## -->
