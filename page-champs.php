<?php
/**
Template name: Page Champs
 */

get_header(); ?>


	<div id="minidossier" class="container">
		<div class="row">

			<section id="primary" class="content-area <?php apprenti_layout_class( 'content' ); ?>">
				<main id="main" class="site-main" role="main">
					
					<div id="post-wrapper" class="post-wrapper post-wrapper-single post-wrapper-page">
					<?php while ( have_posts() ) : the_post(); ?>
<?php insecable();?>
						<?php get_template_part( 'template-parts/content', 'champs' ); ?>

					<?php endwhile; // end of the loop. ?>
					</div><!-- .post-wrapper -->

				</main><!-- #main -->
			</section><!-- #primary -->

			<?php get_sidebar(); ?>

		</div><!-- .row -->
	</div><!-- .container -->

<?php get_footer(); ?>
