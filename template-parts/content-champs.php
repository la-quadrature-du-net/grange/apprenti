<?php
/**
 * The template used for displaying page content in page-champs.php
 *
 * @package apprenti
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="entry-header-wrapper entry-header-wrapper-single">
		<?php if ( apprenti_has_post_edit_link() ) : ?>
		<div class="entry-meta entry-meta-single entry-meta-header-before">
			<?php apprenti_post_edit_link(); ?>
		</div><!-- .entry-meta -->
		<?php endif; ?>

		<header class="entry-header entry-header-single">
			<?php the_title( '<h1 class="title-champs">', '</h1>' ); ?>
		</header><!-- .entry-header -->
		
					<div class="menuchamps">
						<div id="selected" class="analyse"><a href=""><?php esc_html_e('Analysis', 'apprenti')?></a></div>
						<div class="lastart"><a href="last"><?php esc_html_e('Last articles', 'apprenti')?></a></div>
						<div class="revue"><a href="RP"><?php esc_html_e('Press', 'apprenti')?></a></div>
					</div>
<!-- .entry-header-wrapper -->
		<?php the_content(); ?>
		<?php
			wp_link_pages( array(
				'before'      => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'apprenti' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
			) );
		?>
	</div><!-- .entry-content -->
		
	<?php if ( apprenti_has_post_edit_link() ) : ?>
	<footer class="entry-meta entry-meta-single entry-meta-footer">
		<?php apprenti_entry_footer(); ?>
	</footer><!-- .entry-meta -->
	<?php endif; ?>

</article><!-- #post-## -->
