#!/bin/bash -e
#
# Installation via wpcli d'un site wordpress. Ce script a besoin d'un fichier site.env
# passé en premier argument pour définir certaines variables nécessaire au fonctionnement du site.
# 
# Si une variable $DEBUG existe dans l'environnement, ce script considère que l'on est
# en preprod.
# 
[[ -z ${WPCLI_PATH+x} ]] && echo "\$WPCLI_PATH is undefined." && return 1
[[ ! -f ${WPCLI_PATH} ]] && echo "\$WPCLI_PATH does not exist ($WPCLI_PATH)" && return 1

[[ -z ${WP_PATH+x} ]] && echo "\$WP_PATH is undefined." && return 1
[[ ! -d ${WP_PATH} ]] && echo "\$WP_PATH is not a directory or does not exist ($WP_PATH)" && return 1

[[ -z ${1+x} ]] && echo "You need to pass a site.yml file as first and only argument of this script" && return 1
[[ ! -f $1 ]] && echo "$1: does not exist"

WPCLI="php ${WPCLI_PATH} --path=${WP_PATH}"

source $1

# Si on est en PREPROD, utilisons une URL plus simple.
[[ ! -z ${PREPROD+x} ]] && URL="https://${THEME}.grange.dev.lqdn.fr"

# Au cas où, on vérifie que wordpress est bien installé.
$WPCLI core is-installed || echo "Sorry, wordpress isn't installed in $WP_PATH." || return 1

# Créons un site
$WPCLI site create --slug=${THEME} --title="${TITLE}" ${PREPROD:+"--private=true"} --porcelain || echo "Oops, something bad happened." || return 1

# Créons un thème. On crée un .zip à partir de ce qu'il y a dans le répertoire parent
# en n'excluant certains fichiers (le répertoire scripts notamment, et les fichiers commençant par .git)
zip ${THEME}.zip -r * -x .git\* -x scripts
$WPCLI theme install ./${THEME}.zip ${PREPROD:+"--force"} --activate --url=${URL} || echo "Oops install of ${THEME}.zip went bad" || return 1

# Activation des plugins
for PLUGIN in $PLUGINS
do
    $WPCLI plugin activate $PLUGIN --url=${URL} || echo "Can't activate plugin $PLUGIN for site $TITLE"
done
