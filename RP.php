<?php
/**
Template name: Page RP
 */



get_header(); ?>

	<div class="container">
		<div class="row">

			<section id="primary" class="content-area <?php apprenti_layout_class( 'content' ); ?>">
				<main id="main" class="site-main" role="main">
	<header class="entry-header entry-header-single">
			<?php the_title( '<h1 id="titlerp" class="title-champs" style="float:left;">', ' </h1>' ); ?> <a href="<?php  echo esc_url( home_url( '/' ) ); ?><?php esc_html_e('press', 'apprenti')?>"><img class="rs" src="https://www.laquadrature.net/wp-content/uploads/sites/8/2019/02/rss.svg" alt=""/></a>
		</header><!-- .entry-header -->
				<?php if ( have_posts() ) : $i=0; ?>
				
					<div id="post-wrapper" class="post-wrapper post-wrapper-archive" style="clear:both;">
					<?php /* Start the Loop */  ?>
					<?php //query_posts('cat=-61'); while ( have_posts() ) : the_post(); ?>
										<?php// while ( have_posts() ) : the_post(); ?>
<?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;  query_posts("cat=61&paged=$paged"); ?>
<?php while (have_posts()) : the_post(); ?>
						
						<?php
							/* Include the Post-Format-specific template for the content.
							 * If you want to override this in a child theme, then include a file
							 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
							 */
						
							include(locate_template('template-parts/content.php'));
							//get_template_part( 'template-parts/content', get_post_format() );

						?>

					<?php $i++; endwhile; ?>
					</div><!-- .post-wrapper -->

					<?php apprenti_the_posts_pagination(); ?>

				<?php else : ?>

				<?php get_template_part( 'template-parts/content', 'none' ); ?>

				<?php endif; ?>

				</main><!-- #main -->
			</section><!-- #primary -->

			<?php get_sidebar(); ?>

		</div><!-- .row -->
	</div><!-- .container -->

<?php get_footer(); ?>
