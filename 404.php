<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package apprenti
 */

get_header(); ?>

	<div class="container">
		<div class="row">

			<section id="primary" class="content-area <?php apprenti_layout_class( 'content' ); ?>">
				<main id="main" class="site-main" role="main">

					<div id="post-wrapper" class="post-wrapper post-wrapper-single post-wrapper-404">
						<section class="error-404 not-found">
<img style ="	width: 40%;
    margin: 4% 30%;" id="oops" src="https://www.laquadrature.net/wp-content/themes/apprenti/img/<?php echo rand(1, 5);?>.gif">
								<?php the_widget( 'WP_Widget_Search' ); ?>
						</section><!-- .error-404 -->
					</div><!-- .post-wrapper -->

				</main><!-- #main -->
			</section><!-- #primary -->

			<?php get_sidebar(); ?>

		</div><!-- .row -->
	</div><!-- .container -->

<?php get_footer(); ?>
