<?php
/**
 * The template for displaying site header
 * @package apprenti
 */
?>

<header id="masthead" class="site-header" role="banner">
	<div class="container">
		<div class="row">
			<div class="col">

				<div class="site-header-inside-wrapper">
					<div class="site-branding-wrapper">
					
						<?php //the_custom_logo(); ?>
						<div class="site-branding">
							<?php if ( is_front_page() ) : ?>
							
<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
	
<img src="https://www.laquadrature.net/wp-content/themes/apprenti/img/logo.svg" alt="logo" id="logolqdn" >
	<img src="https://www.laquadrature.net/wp-content/themes/apprenti/img/logo2.svg" alt="logo" id="logolqdn2" >

</a>		
<!--<a href="https://laquadrature.net/donner"><img src="<?php// echo esc_url( home_url( '/' ) ); ?>wp-content/themes/apprenti/img/soutien.svg" alt="" id="soutenir" ></a>-->
							<?php else : ?>
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
<img src="https://www.laquadrature.net/wp-content/themes/apprenti/img/logo.svg" alt="" id="logolqdn" >
	<img src="https://www.laquadrature.net/wp-content/themes/apprenti/img/logo2.svg" alt="" id="logolqdn2" >								
</a>
						<!--	<a href="<?php// echo esc_url( home_url( '/' ) ); ?>donner"><img src="https://www.laquadrature.net/wp-content/themes/apprenti/img/soutien.svg" alt="" id="soutenir" ></a>
					-->		
							<?php endif; ?>

							<?php
							$description = get_bloginfo( 'description', 'display' );
							if ( $description || is_customize_preview() ) :
							?>
							<p class="site-description"><?php echo esc_html( $description ); /* WPCS: xss ok. */ ?></p>
							<?php endif; ?>
						</div>
					</div><!-- .site-branding-wrapper -->
				</div><!-- .site-header-inside-wrapper -->

			</div><!-- .col -->
		</div><!-- .row -->
	</div><!-- .container -->
</header><!-- #masthead -->
